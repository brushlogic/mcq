<?php include 'backend/categories-list.php'; ?>
<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="mcq.ico?v=2">

	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css?3" rel="stylesheet">
	<title>MCQ For Engineers</title>
</head>
<body>
    <div class="top-spacer"></div>
    <div class="site-wrapper">
        <div class="content">
            <div class="col-md-1">
                <!--Left margin-->
            </div>
            <div class="col-md-10 central-main">
                <?php include 'include/header.php'; ?>
                <!-- welcome bar -->
                <div class="row">
                    <div class="col-md-12">
                        <p class="menu-subtittle">Welcome to IndiaBIX.com !</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="menu-text">Here, you can read aptitude questions and answers for your interview and entrance exams preparation.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="top-ads">
                            <img src="http://st3.depositphotos.com/1510192/12813/v/950/depositphotos_128132534-stock-illustration-special-price-sale-banner-advertising.jpg" class="img-banner" style="width: 100px;"> 
                            <p>ads here</p>
                        </div>

                        <div class="col-md-12">
                                                       
                            <?php
                            
                            foreach ($catId as $key => $value) {
                                echo '<div class="col-md-6 spaced">
                                    <a href="questions-list.php?catId='.$catId[$key].'&page=1">'.$catName[$key].'</a>
                                </div>';
                                                                     
                            }

                            
                            ?>                            
                        </div>
                        
                    </div>
                    <div class="col-md-2">
                        <img src="http://st3.depositphotos.com/1510192/12813/v/950/depositphotos_128132534-stock-illustration-special-price-sale-banner-advertising.jpg" class="img-banner">   
                        <p>ads here</p>                     
                    </div>
                </div>
                <div class="footer centered">
                    <p>© 2009 - 2017 by IndiaBIX™ Technologies. All Rights Reserved. | Copyright | Terms of Use & Privacy Policy</p>
                    <p>Contact us: info@indiabix.com Follow us on twitter!</p>
                </div>
            </div>
            <div class="col-md-1">
                <!--Right margin-->
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/livesearch.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../js/ie10-viewport-bug-workaround.js"></script> -->
</body>
</html>
