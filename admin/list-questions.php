<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<h3>List of Questions</h3>
<div class="table-responsive">
  	<table class="table">
		<tr>
			<td><b>Question</b></td><td><b>Belongs to Category</b></td><td><b>Date created</b></td><td><b>Date deactivated</b></td><td><b>Options</b></td>
		</tr>
		<?php
		include 'backend/connection.php';
		$sql = "SELECT catName, Q.catId, queCreatedDate, queId, queDeactivatedDate, question
				FROM questions Q, categories C
				WHERE Q.catId = C.catId AND Q.active = '1' AND C.disId = '".$_REQUEST["disId"]."'";
		$result = $conn->query($sql);		

		if ($result->num_rows > 0) {
		    // output data of each row
		    while($row = $result->fetch_assoc()) {

		    	echo "<tr><td>" . $row["question"]. "</td><td>". $row["catName"]."</td><td>" . $row["queCreatedDate"]. "</td><td>" . $row["queDeactivatedDate"]. "</td><td><a href='update-question.php?queid=".$row["queId"]."'><i class='fa fa-pencil fa-fw'></i>Edit</a>  <a href='toggle-question.php?queid=".$row["queId"]."'><i class='fa fa-trash fa-fw'></i>Deactivate</a></td></tr>";
		    			        
		    }
		} else {
		    echo "0 results";
		}
		$conn->close();
		?>
	</table>		
</div>	
<?php include 'include/layout-bottom.php'; ?> 

