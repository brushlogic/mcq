<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<?php
    include 'backend/connection.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT disName,disId FROM disciplines";    

    $result = mysqli_query($conn, $sql);

           
?>
<h3>Update Question</h3>
<p>Change the current discipline for which the question is listed</p>
<form method="POST" action="update-question-2.php">
	
	<div class="row">
        <div class="form-group col-md-4">
            <label for="">Choose discipline</label>
            <input type="hidden" class="form-control" id="queId" name="queId" value="<?php echo $_REQUEST["queid"]?>" required=""></input>	
            <select class="form-control" id="disId" name="disId" required=""  >
                <?php                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each option
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="'.$row["disId"].'">'.$row["disName"].'</option>';                        
                    }
                } else {
                    echo "0 results";
                }

                mysqli_close($conn); 
                ?>

            </select>                       
        </div>
    </div>

	<button class="btn btn-info">Continue</button>
</form>
<?php include 'include/layout-bottom.php'; ?>     