<?php include 'include/layout-top.php'; ?>
<?php
   	include 'backend/connection.php';
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	$sql = "SELECT * FROM categories WHERE catId='".$_REQUEST["catid"]."'";
	

	$result = mysqli_query($conn, $sql);

	$row = $result->fetch_assoc();

	if($row["active"]=='1'){
		echo '<h3>Deactivate category</h3>';
	}else{
		echo '<h3>Restore category</h3>';
	}	
	echo '<form method="POST" action="backend/toggle-category.php">';
		echo '<input type="hidden" name="catId" id="catId" value="'.$_REQUEST["catid"].'">';
		echo '<input type="hidden" name="active" id="active" value="'.$row["active"].'">';
		echo '<div class="row">';
			echo '<p>Are you sure you want to ';
			if($row["active"]=='1'){
				echo 'deactivate';
			}else{
				echo 'restore';
			}
			echo ' the category '.$row["catName"].'?</p>';
			if($row["active"]=='1'){
				echo 'This will deactivate all questions under it';
			}
			
		echo '</div>';
		echo '<button class="btn btn-danger">';
		if($row["active"]=='1'){
			echo 'Deactivate';
		}else{
			echo 'Restore';
		}
		echo '</button>';
	echo '</form>';

	mysqli_close($conn);

    exit();   	
?>
<!-- Aqui va el contenido de la ventana principal -->


<?php include 'include/layout-bottom.php'; ?>     

