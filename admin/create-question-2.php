<?php include 'include/layout-top.php'; ?>
<?php
    include 'backend/connection.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT D.disName, D.disId, catId, catName
            FROM categories C, disciplines D
            WHERE D.disId = C.disId AND D.disId='".$_REQUEST["disId"]."' AND C.active='1'";    

    $result = mysqli_query($conn, $sql);
    $_SESSION['disId']=$_REQUEST["disId"];
?>
<!-- Aqui va el contenido de la ventana principal -->
<h3>Create new question</h3>
<p>Fill in the fields to create a new question</p>
<!-- ESTO REDIRIGE A backend/create-question-2.php -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <?php include 'image-upload.php';?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<form method="POST" action="backend/create-question-2.php" enctype="multipart/form-data">   
    <div class="question-group">    
        <div class="row">
            <div class="form-group col-md-4">            
                <label for="">Select the category in which this question will be listed</label>
                <select class="form-control" id="catId" name="catId[]" required=""  >
                    <?php                
                    if (mysqli_num_rows($result) > 0) {
                        // output data of each option
                        while($row = mysqli_fetch_assoc($result)) {
                            echo '<option value="'.$row["catId"].'" selected="selected">'.$row["catName"].'</option>';                        
                        }
                    } else {
                        echo "0 results";
                    }

                    mysqli_close($conn); 
                    ?>
                </select>
            </div>        
        </div>
        <div class="row">
            <div class="form-group col-md-8">
                <label for="question">Question</label>
                <textarea class="form-control" id="question" name="question[]" required=""></textarea>              
            </div>        
        </div>
        <div class="row">
            <div class="form-group col-md-8">
                <label for="answer1">Option #1</label>
                <input type="text" class="form-control" id="answer1" name="answer1[]" required=""></input>              
            </div>            
        </div>
        <div class="row">
            <div class="form-group col-md-8">
                <label for="answer2">Option #2</label>
                <input type="text" class="form-control" id="answer2" name="answer2[]" required=""></input>              
            </div>            
        </div>
        <div class="row">
            <div class="form-group col-md-8">
                <label for="answer3">Option #3</label>
                <input type="text" class="form-control" id="answer3" name="answer3[]" required=""></input>              
            </div>            
        </div> 
        <div class="row">
            <div class="form-group col-md-8">
                <label for="answer4">Option #4</label>
                <input type="text" class="form-control" id="answer4" name="answer4[]" required=""></input>              
            </div>            
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="correctAnswer">Correct answer</label for="answer">
                <select id="correctAnswer" class="form-control" name="correctAnswer[]" required="">
                    <option value="1">Option #1</option>
                    <option value="2">Option #2</option>
                    <option value="3">Option #3</option>
                    <option value="4">Option #4</option>
                </select>
            </div>
        </div>         
        <div class="row">
            <div class="form-group col-md-8">
                <label for="">Explanation</label>
                <textarea class="form-control" id="explanation" name="explanation[]"></textarea>                          
            </div>
            <div class="form-group col-md-8">
                <label for="btnModal">Image Explanation</label>
                <!-- Trigger the modal with a button -->
                <div id="imgInfo" name=""></div>                
                <button type="button" class="btn btn-info btn-default" name="btnModal" data-toggle="modal" data-target="#myModal">Upload image</button>                         
            </div>        
        </div>    
    </div> 
    <div class="option-group">
        <div class="btn-group">
            <button class="btn btn-primary">Finish</button>
            <button class="btn btn-info" id="oneMore">Create one more question</button>
        </div>
    </div>
</form>

<?php include 'include/layout-bottom.php'; ?>  
<script src="js/multi-questions.js?3"></script> 

