<?php
session_start();
include 'connection.php';
if(isset($_GET['action']))
{          
    if($_GET['action']=="reset")
    {
    	$encrypt = base64_decode($_GET['encrypt']);
    	$sql = "SELECT userId FROM users where userId='".$encrypt."'"; 
    	//echo $sql;
	 	$result = $conn->query($sql);	
	 	$row = $result->fetch_assoc();   
    	if ($result->num_rows <= 0) {    
        	header('Locaton:../index.php?forgot=1&fail=1');
    	}
    	$_SESSION["userId"] = $encrypt;
    }
}else if(isset($_POST['password'])){
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}else{
		$userId = $_SESSION["userId"];
	    $password = $_POST['password'];
		$sql = "SELECT userId FROM users where userId='".$userId."'"; 
		$result = $conn->query($sql);
	    if ($result->num_rows >= 1) { 
	    	$row = $result->fetch_assoc();   
	        $sql = "UPDATE users SET password='".password_hash($password, PASSWORD_DEFAULT)."'  WHERE userId='".$row['userId']."'";
	        //echo $sql;
	        if (mysqli_query($conn, $sql)) {	        	
			    header("Location: ../index.php?forgot=1&reset=1");
			} else {
			    //echo "Error updating record: " . mysqli_error($conn);
			}
	    }else{
	    	header('Locaton:../index.php?forgot=1&fail=1');
	    }

	}
	
	mysqli_close($conn);

}else{
	header('Locaton:../index.php?forgot=1&reset=1');
}
?>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>

	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/login-style.css" rel="stylesheet">
	<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
    <div class="container">
        <div class="card card-container">
        	<!--
            <div class="alert alert-danger alert-dismissable fade in" id="alert-password">
            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            	<strong>Error!</strong> Passwords do not match.
            </div>  
            -->   

            <form class="form-signin" method="POST" action="reset.php">    
                <div class="row" style="margin-bottom: 20px;">
                    <span>Input the new password.</span>
                </div>  
                <div class="row"> 
                	<label for="password">Type in the password</label>
                    <input type="password" id="password" name="password" class="form-control" required autofocus> 
                </div>          
                <div class="row"> 
                	<label for="password">Confirm password</label>
                    <input type="password" id="password2" name="password2" class="form-control" required> 
                </div>
                <button class="btn btn-primary btn-block" >Reset Password</button>
                               
                
            </form><!-- /form -->            
        </div><!-- /card-container -->
    </div><!-- /container -->
    <script type="text/javascript">
    	$('#alert-password').hide();
    	$("#reset").click(function() {
		    mypasswordmatch();
		});
    	function mypasswordmatch()
		{
		    var pass1 = $("#password").val();
		    var pass2 = $("#password2").val();
		    if (pass1 != pass2)
		    {
		    	alert("Passwords do not match");
		    	$('#alert-password').toggle().delay(1000);
		        return false;
		    }
		    else
		    {
		        $( "#reset" ).submit();
		    }
		}    	
    </script>
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/login.js"></script>
</body>
</html>