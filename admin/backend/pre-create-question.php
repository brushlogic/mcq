<?php
session_start(); 
if($_SESSION["usuario"]==null){
    header("Location: ../index.php?fail=1&not-authorized=1");
}

include 'connection.php';
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$array_disciplines = array();

$sql = "SELECT disName,disId FROM disciplines WHERE active='1'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
	// output data of each row
	while($row = $result->fetch_assoc()){
		array_push($array_disciplines, $row["disName"]);		
	}
}
$_SESSION['disciplines'] = $array_disciplines;
//print_r($_SESSION['disciplines']);

$conn->close();
header('location:../create-question.php');
?>