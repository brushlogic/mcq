<?php
	session_start();
    if($_SESSION["usuario"]==null){
        header("Location: ../index.php?fail=1&not-authorized=1");
    }	
   	include 'connection.php';
		// Check connection
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}
		$radio;
		if (isset($_REQUEST['ca']) && $_REQUEST['ca'] == '1') 
		{
		    $radio = 1;
		}
		else if(isset($_REQUEST['ca']) && $_REQUEST['ca'] == '2')
		{
			$radio = 2;
		}
		else if(isset($_REQUEST['ca']) && $_REQUEST['ca'] == '3')
		{
			$radio = 3;
		}
		else if(isset($_REQUEST['ca']) && $_REQUEST['ca'] == '4')
		{
			$radio = 4;
		}else{
			echo 'ERROR';
		}

		$sql = sprintf("INSERT INTO questions(question,catId,answer1,answer2,answer3,answer4,correctAnswer,explanation) VALUES('%s','%s','%s','%s','%s','%s','%s','%s');",
				$_REQUEST["question"],
                $_REQUEST["catId"],
                $_REQUEST["answer1"],
                $_REQUEST["answer2"],
                $_REQUEST["answer3"],
                $_REQUEST["answer4"],
				$radio,
				$_REQUEST["explanation"]
				);
		
		if (mysqli_query($conn, $sql)) {
		    header("Location: ../pre-list-questions.php?success=1&create=1");
		} else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
		
		mysqli_close($conn);

      	exit();
   	
?>