<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<?php
   	include 'backend/connection.php';
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	$sql = "SELECT * FROM disciplines WHERE disId='".$_REQUEST["disid"]."'";
	

	$result = mysqli_query($conn, $sql);

	$row = $result->fetch_assoc();

	mysqli_close($conn);       	
?>
<h3>Update discipline</h3>
<p>Fill in the fields to update an existing discipline</p>
<form method="POST" action="backend/update-discipline.php">
	
	<div class="row">
		<div class="form-group col-md-4">
			<label for="">Discipline name</label>
			<input type="text" class="form-control" id="disName" name="disName" value="<?php echo $row["disName"]?>" required=""></input>				
		</div>
		<div id="resultado" class="col-md-6"></div>	
	</div>
	<div class="row">
		<div class="form-group col-md-4">
			<input type="hidden" class="form-control" id="disId" name="disId" value="<?php echo $row["disId"]?>" required=""></input>			
		</div>
	</div>

	<button class="btn btn-info">Update</button>
</form>
<?php include 'include/layout-bottom.php'; ?>     