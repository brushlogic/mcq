<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<?php
    include 'backend/connection.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT disName,disId FROM disciplines";    

    $result = mysqli_query($conn, $sql);

           
?>
<h3>Update category</h3>
<p>Select a discipline in which to list the category</p>
<form method="POST" action="backend/update-category.php">
	
	<div class="row">
        <div class="form-group col-md-4">
            <label for="">Link category to discipline</label>
            <input type="hidden" name="catName" id="catName" value="<?php echo $_REQUEST["catName"]?>">
            <input type="hidden" name="catId" id="catId" value="<?php echo $_REQUEST["catId"]?>">
            <select class="form-control" id="disId" name="disId" required=""  >
                <?php                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each option
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="'.$row["disId"].'" selected="selected">'.$row["disName"].'</option>';                        
                    }
                } else {
                    echo "0 results";
                }

                mysqli_close($conn); 
                ?>

            </select>                       
        </div>
    </div>

	<button class="btn btn-info">Finish update</button>
</form>
<?php include 'include/layout-bottom.php'; ?>     