<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<?php
    include 'backend/connection.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT catName,catId FROM categories WHERE catId='".$_REQUEST["catid"]."'";    

    $result = mysqli_query($conn, $sql);
    
    $row = mysqli_fetch_assoc($result)
           
?>
<h3>Update discipline</h3>
<p>Fill in the fields to update an existing category</p>
<form method="POST" action="update-category-2.php">
	
	<div class="row">
		<div class="form-group col-md-4">
			<label for="">Category name</label>
			<input type="text" class="form-control" id="catName" name="catName" value="<?php echo $row["catName"]; ?>" required=""></input>				
		</div>
		<div id="resultado" class="col-md-6"></div>	
	</div>	
	<div class="row">
		<div class="form-group col-md-4">
			<input type="hidden" class="form-control" id="catId" name="catId" value="<?php echo $_REQUEST["catid"]?>" required=""></input>			
		</div>
	</div>

	<button class="btn btn-info">Continue</button>
</form>
<?php include 'include/layout-bottom.php'; ?>     