<?php include 'include/layout-top.php'; ?>
<?php
    include 'backend/connection.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT disName, disId FROM disciplines";    

    $result = mysqli_query($conn, $sql);


    if(isset($_REQUEST["success"])){
        echo '<div class="alert alert-success alert-dismissable fade in">';
        echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        if(isset($_REQUEST["toggled"])){
            echo '<strong>Success!</strong> One question has changed its status.';
        }else if(isset($_REQUEST["create"])){
            echo '<strong>Success!</strong> The question has been created succesfully.';
        }else if(isset($_REQUEST["update"])){
            echo '<strong>Success!</strong> The question has been updated succesfully.';
        }
        echo '</div>';
    }
    

?>
<!-- Aqui va el contenido de la ventana principal -->
<h3>Select a discipline</h3>
<form method="POST" action="list-questions.php">
	<div class="row">
        <div class="form-group col-md-4">
            <label for="">Select a discipline to list the questions under it</label>
            <select class="form-control" id="disId" name="disId" required=""  >
                <?php                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each option
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="'.$row["disId"].'" selected="selected">'.$row["disName"].'</option>';                        
                    }
                } else {
                    echo "0 results";
                }

                mysqli_close($conn); 
                ?>
            </select>                       
        </div>        
    </div>
	
	<button class="btn btn-primary">Continue</button>
</form>
<?php include 'include/layout-bottom.php'; ?>     

<script type="text/javascript">
//codigo para chequear si la question ya esta registrada

$(document).ready(function(){
                         
    var consulta;
             
    //hacemos focus
    $("#question").focus();
                                                 
    //comprobamos si se pulsa una tecla
    $("#question").keyup(function(e){
        //obtenemos el texto introducido en el campo
        consulta = $("#question").val();
                                      
        //hace la búsqueda
        $("#resultado").delay(1000).queue(function(n) {      
                                           
            $("#resultado").html('<img src="img/ajax-loader.gif" />');
                                           
            $.ajax({
                type: "POST",
                url: "backend/check-question.php",
                data: "b="+consulta,
                dataType: "html",
                error: function(){
                    alert("Ajax error.");
                },
                success: function(data){                                                    
                    $("#resultado").html(data);
                    n();
                }
            });
                                           
        });
                                
    });
                          
});
</script>