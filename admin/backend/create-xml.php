<?php 
session_start();
    if($_SESSION["usuario"]==null){
        header("Location: ../index.php?fail=1&not-authorized=1");
    }
	$DS = DIRECTORY_SEPARATOR;
	$back = '..' . $DS;
	$source = 'fluidxml\source';
	\set_include_path($source . PATH_SEPARATOR . \get_include_path());

	////////////////////////////////////////////////////////////////////////////////


	require_once 'FluidXml.php';

	use \FluidXml\FluidXml;
	use \FluidXml\FluidNamespace;
	use function \FluidXml\fluidxml;
	use function \FluidXml\fluidns;
	use function \FluidXml\fluidify;


	$doc = fluidxml();

	$doc->add('words', true)
	        	->add('word', 'Computer Arquitecture', ['url' => '/mcq/questions-list.php?catId=3'])
	        	->add('word', 'Special Calculus', ['url' => '/mcq/questions-list.php?catId=7'])
	        	;


	$doc->save('../../tmp/search.xml');
?>