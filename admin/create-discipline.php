<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<h3>Create new discipline</h3>
<p>Fill in the fields to create a new discipline</p>
<form method="POST" action="backend/create-discipline.php">
	<div class="row">
		<div class="form-group col-md-4">
			<label for="">Discipline name</label>
			<input type="text" class="form-control" id="disName" name="disName" required=""></input>				
		</div>
		<div id="resultado" class="col-md-6"></div>	
	</div>
	
	<button class="btn btn-primary">Create</button>
</form>
<?php include 'include/layout-bottom.php'; ?>     

<script type="text/javascript">
//codigo para chequear si la discipline ya esta registrada
$(document).ready(function(){
                         
    var consulta;
             
    //hacemos focus
    $("#disName").focus();
                                                 
    //comprobamos si se pulsa una tecla
    $("#disName").keyup(function(e){
        //obtenemos el texto introducido en el campo
        consulta = $("#disName").val();
                                      
        //hace la búsqueda
        $("#resultado").delay(1000).queue(function(n) {      
                                           
            $("#resultado").html('<img src="img/ajax-loader.gif" />');
                                           
            $.ajax({
                type: "POST",
                url: "backend/check-discipline.php",
                data: "b="+consulta,
                dataType: "html",
                error: function(){
                    alert("Ajax error.");
                },
                success: function(data){                                                    
                    $("#resultado").html(data);
                    n();
                }
            });
                                           
        });
                                
    });
                          
});
</script>