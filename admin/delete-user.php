<?php include 'include/layout-top.php'; ?>
<?php
   	include 'backend/connection.php';
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	$sql = "SELECT * FROM users WHERE userId='".$_REQUEST["userid"]."'";
	

	$result = mysqli_query($conn, $sql);

	$row = $result->fetch_assoc();

	echo '<h3>Delete user</h3>';
	echo '<form method="POST" action="backend/delete-users.php">';
		echo '<input type="hidden" name="userId" id="userId" value="'.$_REQUEST["userid"].'">';
		echo '<div class="row">';
			echo '<p>Are you sure you want to delete the user '.$row["name"].'?</p>';
			echo '<p>You cannot undo this action.</p>';
		echo '</div>';
		echo '<button class="btn btn-danger">Delete</button>';
	echo '</form>';

	mysqli_close($conn);

    exit();   	
?>
<!-- Aqui va el contenido de la ventana principal -->


<?php include 'include/layout-bottom.php'; ?>     

