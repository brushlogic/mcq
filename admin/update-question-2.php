<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<?php
    include 'backend/connection.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT D.disName, D.disId, catId, catName
            FROM categories C, disciplines D
            WHERE D.disId = C.disId AND D.disId='".$_REQUEST["disId"]."' AND C.active='1'";    

    $result = mysqli_query($conn, $sql);

?>
<h3>Update Question</h3>
<p>Fill in the fields to update an existing question</p>
<form method="POST" action="update-question-3.php">
	<input type="hidden" class="form-control" id="queId" name="queId" value="<?php echo $_REQUEST["queId"]?>" required=""></input>
	<div class="row">
        <div class="form-group col-md-4">            
            <label for="">Select the category in which this question will be listed</label>
            <select class="form-control" id="catId" name="catId" required=""  >
                <?php                
                if (mysqli_num_rows($result) > 0) {
                    // output data of each option
                    while($row = mysqli_fetch_assoc($result)) {
                        echo '<option value="'.$row["catId"].'">'.$row["catName"].'</option>';                        
                    }
                } else {
                    echo "0 results";
                }
                mysqli_close($conn); 
                ?>
            </select>
        </div>        
    </div>

	<button class="btn btn-info">Continue</button>
</form>
<?php include 'include/layout-bottom.php'; ?>     