<?php
    session_start();
    if(isset($_SESSION["usuario"])){
        header("Location: admin.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/login-style.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
    <div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <!--
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            -->            
            <form class="form-signin" action="backend/forgot.php" method="POST">    
                <div class="row" style="margin-bottom: 20px;">
                    <span>Input the email you used to register and we will send you an email with details to recover your account.</span>
                </div>  
                <div class="row"> 
                    <input type="email" id="email" name="email" class="form-control" placeholder="example@mail.com" required autofocus> 
                </div>          
                <div class="row"> 
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Send email</button> 
                </div> 
                               
                
            </form><!-- /form -->            
        </div><!-- /card-container -->
    </div><!-- /container -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/login.js"></script>
</body>
</html>