<?php include 'include/layout-top.php'; ?>
<?php
    include 'backend/connection.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT * FROM `questions` WHERE `queId` = '".$_REQUEST["queId"]."'";    

    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $signi= $row['correctAnswer']; 
?>
<!-- Aqui va el contenido de la ventana principal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <?php include 'image-upload-Upd.php';?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<h3>Update Question</h3>
<p>Fill in the fields to update the question</p>
<form method="POST" action="backend/update-question.php">
    <input type="hidden" name="queId" id="queId" value="<?php echo $_REQUEST["queId"]; ?>"><input type="hidden" name="catId" id="catId" value="<?php echo $_REQUEST["catId"]; ?>"> 
    <div class="row">
        <div class="form-group col-md-8">
            <label for="">Question</label>
            <textarea class="form-control" id="question" name="question" required=""><?php echo $row["question"]; ?></textarea>              
        </div>        
    </div>
    <div class="row">
        <div class="form-group col-md-8">
            <label for="">Option #1</label>
            <input type="text" class="form-control" id="answer1" name="answer1" required="" value="<?php echo $row["answer1"]; ?>"></input>              
        </div>
        <div id="resultado" class="col-md-4">
            <label for="answer1">Correct answer #1</label for="answer">

            <input type="radio" name="ca" value="1" required <?php if ($signi==1) echo 'checked=""';?>>
        </div> 
    </div>
    <div class="row">
        <div class="form-group col-md-8">
            <label for="">Option #2</label>
            <input type="text" class="form-control" id="answer2" name="answer2" required="" value="<?php echo $row["answer2"]; ?>"></input>              
        </div>
        <div id="resultado" class="col-md-4">
            <label for="answer2">Correct answer #2</label for="answer">
            <input type="radio" name="ca" value="2" <?php if ($signi==2) echo 'checked=""';?>>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-8">
            <label for="">Option #3</label>
            <input type="text" class="form-control" id="answer3" name="answer3" required="" value="<?php echo $row["answer3"]; ?>"></input>              
        </div>
        <div id="resultado" class="col-md-4">
            <label for="answer3">Correct answer #3</label for="answer">
            <input type="radio" name="ca" value="3" <?php if ($signi==3) echo 'checked=""';?>>
        </div>
    </div> 
    <div class="row">
        <div class="form-group col-md-8">
            <label for="">Option #4</label>
            <input type="text" class="form-control" id="answer4" name="answer4" required="" value="<?php echo $row["answer4"]; ?>"></input>              
        </div>
        <div id="resultado" class="col-md-4">
            <label for="answer4">Correct answer #4</label for="answer">
            <input type="radio" name="ca" value="4" <?php if ($signi==4) echo 'checked=""';?>>
        </div>
    </div> 
    <div class="row">
        <div class="form-group col-md-8">
            <label for="">Explanation</label>
            <textarea class="form-control" id="explanation" name="explanation" required=""><?php echo $row["explanation"]; ?></textarea>                          
        </div>    
        <div class="form-group col-md-8">
            <label for="btnModal">Image Explanation</label>
            <!-- Trigger the modal with a button -->
            <div id="imgInfo">
                <?php if(isset($row["imgName"])){ ?>
                <input type='text' name='imgName' id='imgName' value='<?php echo $row["imgName"]; ?>' readonly=''>
                <?php }else{echo 'No image uploaded';} ?>
            </div>                
            <button type="button" class="btn btn-info btn-default" name="btnModal" data-toggle="modal" data-target="#myModal">Upload image</button>                         
        </div>     
    </div>    
    
    <button class="btn btn-primary">Update</button>
</form>
<?php
    }else{
        echo 'Something went wrong';
    } 
?>
<?php include 'include/layout-bottom.php'; ?>   

