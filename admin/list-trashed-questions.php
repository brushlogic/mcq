<?php include 'include/layout-top.php'; ?>
<!-- Aqui va el contenido de la ventana principal -->
<?php
if(isset($_REQUEST["success"])){
	echo '<div class="alert alert-success alert-dismissable fade in">';
	echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
	if(isset($_REQUEST["toggled"])){
		echo '<strong>Success!</strong> One question has changed its status.';
	}else if(isset($_REQUEST["create"])){
		echo '<strong>Success!</strong> The question has been created succesfully.';
	}else if(isset($_REQUEST["update"])){
		echo '<strong>Success!</strong> The question has been updated succesfully.';
	}
	echo '</div>';
}
?>
<h3>List of Users</h3>
<div class="table-responsive">
  	<table class="table">
		<tr>
			<td><b>Question Name</b></td><td><b>Date created</b></td><td><b>Date deactivated</b></td><td><b>Options</b></td>
		</tr>
		<?php
		include 'backend/connection.php';
		$sql = "SELECT * FROM questions WHERE active='0'";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		        echo "<tr><td>" . $row["question"]."</td><td>" . $row["queCreatedDate"]. "</td><td>" . $row["queDeactivatedDate"]. "</td>";		                
		        echo "<td><a href='update-question.php?queid=".$row["queId"]."'><i class='fa fa-pencil fa-fw'></i>Edit</a>  <a href='toggle-question.php?queid=".$row["queId"]."'><i class='fa fa-trash fa-fw'></i>Restore</a></td></tr>";
		    }
		} else {
		    echo "0 results";
		}
		$conn->close();
		?>
	</table>		
</div>	
<?php include 'include/layout-bottom.php'; ?>   

