<?php include 'include/layout-top.php'; ?>
<?php
   	include 'backend/connection.php';
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	$sql = "SELECT * FROM disciplines WHERE disId='".$_REQUEST["disid"]."'";
	

	$result = mysqli_query($conn, $sql);

	$row = $result->fetch_assoc();

	if($row["active"]=='1'){
		echo '<h3>Deactivate discipline</h3>';
	}else{
		echo '<h3>Restore discipline</h3>';
	}	
	echo '<form method="POST" action="backend/toggle-discipline.php">';
		echo '<input type="hidden" name="disId" id="disId" value="'.$_REQUEST["disid"].'">';
		echo '<input type="hidden" name="active" id="active" value="'.$row["active"].'">';
		echo '<div class="row">';
			echo '<p>Are you sure you want to ';
			if($row["active"]=='1'){
				echo 'deactivate';
			}else{
				echo 'restore';
			}
			echo ' the discipline '.$row["disName"].'?</p>';
			if($row["active"]=='1'){
				echo 'This will deactivate all categories and questions under it';
			}			
		echo '</div>';
		echo '<div class="row"><button class="btn btn-danger">';
		if($row["active"]=='1'){
			echo 'Deactivate';
		}else{
			echo 'Restore';
		}
		echo '</button></div>';
	echo '</form>';

	mysqli_close($conn);

    exit();   	
?>
<!-- Aqui va el contenido de la ventana principal -->


<?php include 'include/layout-bottom.php'; ?>     

