<!--
<html>
<head>
	<title>Ajax Image Upload Using PHP and jQuery</title>
	
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	
	-->
	<link rel="stylesheet" href="css/imgUpload.css?1" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/imgUploadUpd.js?3"></script>
	<!--
</head>
<body>
-->
	<div class="col-md-12">		
		<hr>
		<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
			<div class="image_preview form-group"><img id="previewing" src="img/uploading.png" width="200" /></div>
			<hr id="line">
			<div>
				<label>Select Your Image</label><br/>
				<input type="file" name="file" id="file" required />
				<input type="submit" value="Upload" class="btn btn-info btn-default form-control" />
			</div>

			
		</form>
		<h4 id='loading' >loading..<img src="img/ajax-loader.gif"></h4>
		<div id="message"></div>
	</div>
	
<!--
</body>
</html>
-->