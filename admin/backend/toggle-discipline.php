<?php
	session_start();
	if($_SESSION["usuario"]==null){
	    header("Location: ../index.php?fail=1&not-authorized=1");
	}
   	include 'connection.php';
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	// sql to update a record
	/*
	$timezone = date_default_timezone_get(); 
	date_default_timezone_set($timezone);
	*/
	date_default_timezone_set("Asia/Calcutta");
	$date = date('Y/m/d h:i:s', time());

	if($_REQUEST["active"] == '0'){
		$sql = "UPDATE disciplines SET active='1', disDeactivatedDate=''  WHERE disId=".$_REQUEST["disId"]."";
	}else{
		$sql = "UPDATE disciplines SET active='0', disDeactivatedDate='".$date."'  WHERE disId=".$_REQUEST["disId"]."";		
	}

	if (mysqli_query($conn, $sql)) {
	    header("Location: ../list-disciplines.php?success=1&toggled=1");
	} else {
	    echo "Error updating record: " . mysqli_error($conn);
	}

	mysqli_close($conn);

    exit();   	
?>