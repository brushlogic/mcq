<?php
    session_start();
    if($_SESSION["usuario"]==null){
        header("Location: ../index.php?fail=1&not-authorized=1");
    }
    include 'connection.php';
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	$sql = sprintf("INSERT INTO users(name,username,email,privileges,password) VALUES('%s','%s','%s','%s','%s');",
            $_REQUEST["name"],
            $_REQUEST["username"],
            $_REQUEST["email"],
            $_REQUEST["privileges"],
            password_hash($_REQUEST["password"], PASSWORD_DEFAULT)				
			);
	if (mysqli_query($conn, $sql)) {
	    header("Location: ../list-users.php?success=1&create=1");
	} else {
	    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
	}
    mysqli_close($conn);

    exit();
   	
?>