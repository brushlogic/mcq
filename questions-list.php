<?php include 'backend/questions-list.php'; ?>
<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="mcq.ico?v=2">

	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css?10" rel="stylesheet">
    <link href="css/pagination.css" rel="stylesheet">
	<title>MCQ For Engineers</title>
</head>
<body>
    <div class="top-spacer"></div>
    <div class="site-wrapper">
        <div class="content">
            <div class="col-md-1">
                <!--Left margin-->
            </div>
            <div class="col-md-10 central-main">
                <?php include 'include/header.php'; ?>
                <!-- welcome bar -->
                <div class="row">
                    <div class="col-md-12">
                        <p class="menu-subtittle">Welcome to IndiaBIX.com !</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="menu-text">Here, you can read aptitude questions and answers for your interview and entrance exams preparation.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="top-ads">
                            <img src="http://st3.depositphotos.com/1510192/12813/v/950/depositphotos_128132534-stock-illustration-special-price-sale-banner-advertising.jpg" class="img-banner" style="width: 100px;"> 
                            <p>ads here</p>
                        </div>                        
                        <div class="col-md-12 col-lg-12">
                                                       
                            <?php
                            
                            foreach ($queId as $key => $value) {
                                //Renaming the answer
                                if($correctAnswer[$key]=='1'){
                                    $correctAnswer[$key]='A';
                                }else if($correctAnswer[$key]=='2'){
                                    $correctAnswer[$key]='B';
                                }else if($correctAnswer[$key]=='3'){
                                    $correctAnswer[$key]='C';
                                }else if($correctAnswer[$key]=='4'){
                                    $correctAnswer[$key]='D';
                                }



                                $questionNumber = $key+1;                                
                                echo '<div class="col-md-6 spaced">
                                        <div class="row">
                                            <span>'.$questionNumber.'.</span><span class="question">'.$question[$key].'</span>
                                        </div>

                                    </div>
                                    <div class="row spaced">
                                        <div class="col-md-12 ">     
                                            <span class="question-letter"><a href="javascript:void(0);" id="A'.$key.'">A</a></span><span class="question">'.$answer1[$key].'</span>
                                            <span id="correctA'.$key.'" class="correct"><img src="http://www.indiabix.com/_files/images/website/accept.png"></span>
                                        </div>
                                    </div>
                                    <div class="row spaced">
                                        <div class="col-md-12 ">     
                                            <span class="question-letter"><a href="javascript:void(0);" id="B'.$key.'">B</a></span><span class="question">'.$answer2[$key].'</span>
                                            <span id="correctB'.$key.'" class="correct"><img src="http://www.indiabix.com/_files/images/website/accept.png"></span>
                                        </div>
                                    </div>
                                    <div class="row spaced">
                                        <div class="col-md-12 ">     
                                            <span class="question-letter"><a href="javascript:void(0);" id="C'.$key.'">C</a></span><span class="question">'.$answer3[$key].'</span>
                                            <span id="correctC'.$key.'" class="correct"><img src="http://www.indiabix.com/_files/images/website/accept.png"></span>
                                        </div>
                                    </div>
                                    <div class="row spaced">
                                        <div class="col-md-12 ">     
                                            <span class="question-letter"><a href="javascript:void(0);" id="D'.$key.'">D</a></span><span class="question">'.$answer4[$key].'</span>
                                            <span id="correctD'.$key.'" class="correct"><img src="http://www.indiabix.com/_files/images/website/accept.png"></span>
                                        </div>
                                    </div> 
                                    <div id="answers'.$key.'" class="answers">
                                        
                                        <div class="row spaced">
                                            <div class="col-md-12 ">     
                                                <span class="question answer-letter">Answer:</span><span> Option </span><span><b>'.$correctAnswer[$key].'</b></span> 
                                            </div>
                                        </div>
                                         
                                        ';
                                        if($explanation[$key]){
                                        echo '
                                        <div class="row spaced">
                                            <div class="col-md-12 ">     
                                                <span class="question answer-letter">Explanation:</span><span> '.$explanation[$key].'</span> 
                                            </div>
                                        </div>
                                        ';
                                        }                                        
                                        if($imgName[$key]){
                                        echo ' 
                                        <div class="row spaced">
                                            <img class="imgExplanation" src="admin/img/explanation/'.$imgName[$key].'">
                                        </div>
                                        ';
                                        }
                                        echo '</div>
                                    <div class="row spaced">
                                        <div class="col-md-12 ">     
                                            <span class="question answer-letter"><a href="javascript:void(0);" id="R'.$key.'">View Answer</a></span>
                                        </div>
                                    </div> 
                                    <input type="hidden" name="respuesta'.$key.'" id="respuesta'.$key.'" value="'.$correctAnswer[$key].'">
                                    <hr>
                                    ';

                                                                     
                            }

                            
                            ?>
                               
                            
                                                   
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                echo $pagination; 
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <img src="http://st3.depositphotos.com/1510192/12813/v/950/depositphotos_128132534-stock-illustration-special-price-sale-banner-advertising.jpg" class="img-banner">   
                        <p>ads here</p>                     
                    </div>
                </div>
                <div class="footer centered">
                    <p>© 2009 - 2017 by IndiaBIX™ Technologies. All Rights Reserved. | Copyright | Terms of Use & Privacy Policy</p>
                    <p>Contact us: info@indiabix.com Follow us on twitter!</p>
                </div>
            </div>
            <div class="col-md-1">
                <!--Right margin-->
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/questions.js?2"></script>
    <script src="js/livesearch.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../js/ie10-viewport-bug-workaround.js"></script> -->
</body>
</html>
