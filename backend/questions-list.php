<?php 
session_start(); 
include 'admin/backend/connection.php'; 
$queId=array();
$question=array();
$answer1=array();
$answer2=array();
$answer3=array();
$answer4=array();
$correctAnswer=array();
$explanation=array();
$imgName=array();
if(isset($_GET['catId'])){
	//SI NO HE BUSCADO catCIPLINA BUSCALAS
	$sessioncatId = $_GET['catId'];	
	$sql = "SELECT * FROM questions WHERE active='1' AND catId='".$sessioncatId."'";
	$result = $conn->query($sql);
	
		
	/*
	print_r($queId);
	echo '<br>';
	print_r($question);
	echo '<br>';
	*/
	//PAGINATION
	$total = mysqli_num_rows($result);

	$adjacents = 3;
	$targetpage = "questions-list.php?catId=".$sessioncatId;
	$limit = 6; //how many items to show per page
	$page = $_GET['page'];
	if($page){ 
		$start = ($page - 1) * $limit; //first item to display on this page
	}else{
		$start = 0;	
	}
	/* Setup page vars for display. */
	if ($page == 0) $page = 1; //if no page var is given, default to 1.
	$prev = $page - 1; //previous page is current page - 1
	$next = $page + 1; //next page is current page + 1
	$lastpage = ceil($total/$limit); //lastpage.
	$lpm1 = $lastpage - 1; //last page minus 1

	$sql2 = "SELECT * FROM questions WHERE active='1' AND catId='".$sessioncatId."'";
	$sql2 .= " order by queId desc limit $start ,$limit ";
	$sql_query = mysqli_query($conn,$sql2); 
	$curnm = mysqli_num_rows($sql_query);

	if ($sql_query->num_rows > 0) {
		// output data of each row
		while($row = $sql_query->fetch_assoc()) {	

			array_push($queId, $row["queId"]);
			array_push($question, $row["question"]);
			array_push($answer1, $row["answer1"]);
			array_push($answer2, $row["answer2"]);
			array_push($answer3, $row["answer3"]);
			array_push($answer4, $row["answer4"]);
			array_push($correctAnswer, $row["correctAnswer"]);
			array_push($explanation, $row["explanation"]);
			array_push($imgName, $row["imgName"]);	

		}

	} else {
		echo "No questions registered in this category";
	}
	
	/* CREATE THE PAGINATION */
	$counter=0;
	$pagination = "";
	if($lastpage > 1)
	{ 
		$pagination .= "<div> <ul class='pagination'>";
		if ($page > $counter+1) {
			$pagination.= "<li><a href=\"$targetpage&page=$prev\">prev</a></li>"; 
		}
		if ($lastpage < 7 + ($adjacents * 2)) 
		{ 
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
				$pagination.= "<li class='active'><a href='#' class='active'>$counter</a></li>";
				else
				$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2)) //enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2)) 
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
					$pagination.= "<li><a href='#' class='active'>$counter</a></li>";
					else
					$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
				}
				$pagination.= "<li>...</li>";
				$pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>"; 
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<li><a href=\"$targetpage&page=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetpage&page=2\">2</a></li>";
				$pagination.= "<li>...</li>";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><a href='#' class='active'>$counter</a></li>";
					else
						$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
				}
				$pagination.= "<li>...</li>";
				$pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>"; 
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<li><a href=\"$targetpage&page=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetpage&page=2\">2</a></li>";
				$pagination.= "<li>...</li>";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; 
				$counter++)
				{
					if ($counter == $page)
					$pagination.= "<li><a href='#' class='active'>$counter</a></li>";
					else
					$pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>"; 
				}
			}
		}

		//next button
		if ($page < $counter - 1) 
			$pagination.= "<li><a href=\"$targetpage&page=$next\">next</a></li>";
		else
			$pagination.= "";
		$pagination.= "</ul></div>\n"; 
	}

	$conn->close();

}else{
	header("location: ../index.php");
}

