<?php session_start(); 
if($_SESSION["usuario"]==null){
    header("Location: index.php?fail=1&not-authorized=1");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Panel</title>


    <link rel="icon" href="../mcq.ico?v=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/metisMenu.min.css" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    

</head>

<body>

    <div id="wrapper">

        
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin.php">Admin Panel</a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                 <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $_SESSION["usuario"];?><i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                    <!--
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>

                        <li class="divider"></li>
                        -->
                        <li><a href="backend/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <?php 
                        if($_SESSION["privileges"]=='2'){
                        ?>
                        <li>
                        <!-- mostrar esto si el usuario es administrador -->
                            <a href="/#"><i class="fa fa-users fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="create-user.php"><i class='fa fa-plus fa-fw'></i> Add new User</a>
                                </li>
                                <li>
                                    <a href="list-users.php"><i class='fa fa-list-ol fa-fw'></i> User list</a>
                                </li>
                            </ul>
                        </li>
                        <!-- hasta aqui -->
                        <?php   
                        }
                        ?>

                        <li>
                            <a href="/#"><i class="fa fa-mortar-board fa-fw"></i> Disciplines<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="create-discipline.php"><i class='fa fa-plus fa-fw'></i> Add new Discipline</a>
                                </li>
                                <li>
                                    <a href="list-disciplines.php"><i class='fa fa-list-ol fa-fw'></i> Disciplines list</a>
                                </li>
                                <li>
                                    <a href="list-trashed-disciplines.php"><i class='fa fa-trash fa-fw'></i> Trashed Disciplines</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="/#"><i class="fa fa-sitemap fa-fw"></i> Categories<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="create-category.php"><i class='fa fa-plus fa-fw'></i> Add new Category</a>
                                </li>
                                <li>
                                    <a href="list-categories.php"><i class='fa fa-list-ol fa-fw'></i> Categorie List</a>
                                </li>
                                <li>
                                    <a href="list-trashed-categories.php"><i class='fa fa-trash fa-fw'></i> Trashed Categories</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="/#"><i class="fa fa-check-square fa-fw"></i> Questions<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="backend/pre-create-question.php"><i class='fa fa-plus fa-fw'></i> Add new Question</a>
                                </li>
                                <li>
                                    <a href="pre-list-questions.php"><i class='fa fa-list-ol fa-fw'></i> Questions List</a>
                                </li>
                                <li>
                                    <a href="list-trashed-questions.php"><i class='fa fa-trash fa-fw'></i> Trashed Questions</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <a href="/#"><i class="fa fa-gear fa-fw"></i> Settings<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="config.php"><i class='fa fa-gears fa-fw'></i> Site Configuration</a>
                                </li>                                
                            </ul>
                        </li>
                                            
                    </ul>
                </div>
            </div>
     </nav>
        <div id="page-wrapper">