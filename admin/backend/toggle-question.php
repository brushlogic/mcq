<?php
	session_start();
	if($_SESSION["usuario"]==null){
	    header("Location: ../index.php?fail=1&not-authorized=1");
	}
   	include 'connection.php';
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}
	// sql to update a record
	/*
	$timezone = date_default_timezone_get(); 
	date_default_timezone_set($timezone);
	*/
	date_default_timezone_set("Asia/Calcutta");
	$date = date('Y/m/d h:i:s', time());

	if($_REQUEST["active"] == '0'){
		$sql = "UPDATE questions SET active='1', queDeactivatedDate=''  WHERE queId=".$_REQUEST["queId"]."";
	}else{
		$sql = "UPDATE questions SET active='0', queDeactivatedDate='".$date."'  WHERE queId=".$_REQUEST["queId"]."";		
	}

	if (mysqli_query($conn, $sql)) {
	    header("Location: ../pre-list-questions.php?success=1&toggled=1");
	} else {
	    echo "Error updating record: " . mysqli_error($conn);
	}

	mysqli_close($conn);

    exit();   	
?>