<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!--
        <li class="dropdown">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <img src="img/menu-black.png" width="20"> <span class="caret"></span></a>
          <ul class="dropdown-menu auto-width">
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
            <li><p class="menu-subtittle">Subtittle</p></li>
            <li>
              <a href="" style="display: inline-block;">Link</a> 
              <a href="" style="display: inline-block;">Link</a>
              <a href="" style="display: inline-block;">Link</a>
            </li>
            <li role="separator" class="divider"></li>
          </ul>
        </li>
        -->

        <li><a href="/">Home</a></li>
        <!--
        <li><a href="#">Aptitude</a></li>
        <li><a href="#">Logical</a></li>
        <li><a href="#">Verbal</a></li>
        <li><a href="#">Current Affairs</a></li>
        <li><a href="#">GK</a></li>
        <li><a href="#">Engineering</a></li>
        <li><a href="#">Online Tests</a></li>
        <li><a href="#">Puzzles</a></li>
        <li><a href="#">Ask Now!</a></li>
        -->
        <?php
        //echo $_SESSION['disId'];
        foreach ($_SESSION['disId'] as $key3 => $value3) {
          echo '<li><a href="categories-list.php?disid='.$value3.'">'.$_SESSION['disName'][$key3].'</a></li>';  
        }
        ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>