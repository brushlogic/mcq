<?php	
	session_start();
    if($_SESSION["usuario"]==null){
        header("Location: ../index.php?fail=1&not-authorized=1");
    }
   	include 'connection.php';
		// Check connection
		if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
		}
		$catName = $_REQUEST["catName"];
		$catId = $_REQUEST["disId"];
		$sql = sprintf("INSERT INTO categories(catName,disId) VALUES('%s','%s');",
				$catName,
                $catId				
				);		

		if (mysqli_query($conn, $sql)) {
		    header("Location: update-xml.php?create=1&catName=".$catName."&catId=".$catId);
		} else {
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}

		mysqli_close($conn);

      	exit();
   	
?>